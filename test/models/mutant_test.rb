require 'test_helper'

class MutantTest < ActiveSupport::TestCase
  test "should not save mutants with name longer than 30 characters" do
    m = Mutant.new :name => 'cyclopsangelarielbeastblinkstor'
    assert_not m.valid?
    assert_equal 1, m.errors.count
    assert_equal "Name is too long (maximum is 30 characters)", m.errors.full_messages_for(:name).first
  end

  test "should not save mutants with anything other than characters and underscores in name" do
    m = Mutant.new :name => 'cyclo#ps'
    assert_not m.valid?
    assert_equal 1, m.errors.count
    assert_equal "Name can only contain alphabets, numbers, and underscores(_)", m.errors.full_messages_for(:name).first
  end

  test "should save a mutant with alphanum characters in it" do
    m = Mutant.new :name => 'cyclops12'
    assert m.valid?
    assert m.save
  end

  test "should save mutants with underscores" do
    m = Mutant.new :name => 'cyclops_12'
    assert m.valid?
    assert m.save
  end

  test "test mutant search" do
    set_data
    assert_equal 10, Mutant.count

    cyc_search = Mutant.search_for_team(@team, 'Cyc', 1)
    assert_equal 1, cyc_search.count
    assert_equal "Cyclops", cyc_search.first[:name]

    b_search = Mutant.search_for_team(@team, 'B', 1)
    assert_equal 2, b_search.count
    assert_equal ["Beast", "Blink"], b_search.collect{|mutant| mutant[:name]}.sort
  end

  private

  def set_data
    [Mutant, Team, TeamMember].each{|klass| klass.destroy_all}
    @team = Team.create :name => 'Team X'
    members = ["Angel", "Ariel", "Armor", "Aurora", "Iceman", "Beast", "Blink", "Cyclops", "Storm", "Rogue"]
    @mutants = members.collect{|name| Mutant.create :name => name}
    @mutants[0...5].each do |mutant| #First 5 should not be returned in the search
      @team.mutants << mutant
    end
    @team.save
  end
end
