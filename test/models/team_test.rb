require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  test "should not save teams with name longer than 30 characters" do
    t = Team.new :name => 'TeamX TeamS TeamY TeamZ TeamA TeamB TeamC TeamD'
    assert_not t.valid?
    assert_equal 1, t.errors.count
    assert_equal "Name is too long (maximum is 30 characters)", t.errors.full_messages_for(:name).first
  end

  test "should not save teams with empty name" do
    t = Team.new
    assert_not t.valid?
    assert_equal 1, t.errors.count
    assert_equal "Name can't be blank", t.errors.full_messages_for(:name).first
  end

  test "test team search" do
    set_data
    assert_equal 10, Team.count

    team_search = Team.search_for_task(@task, 'Tea', 1)
    assert_equal 5, team_search.count
    assert_equal ["Team10", "Team6", "Team7", "Team8", "Team9"], team_search.collect{|s| s[:name]}.sort
  end

  private

  def set_data
    [Task, Team, TeamTask].each{|klass| klass.destroy_all}
    @task = Task.create :name => 'Kill them all'
    @teams = (1..10).collect{|i| Team.create :name => "Team#{i}"}
    @teams[0...5].each do |team| #First 5 should not be returned in the search
      @task.teams << team
    end
    @task.save
  end
end
