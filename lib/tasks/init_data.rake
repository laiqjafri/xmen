namespace :init_data do
  desc "Add mutants"
  task :mutants => :environment do
    ["Angel", "Ariel", "Armor", "Aurora", "Beast", "Blink", "Box", "Chamber", "Cyclops", "Firestar", "Frenzy", "Emma Frost", "Iceman", "Jubilee", "Karma", "Lifeguard", "Lockheed", "M", "Magik", "Magma", "Magneto", "Marvel Girl", "Moonstar", "Nightcrawler", "Northstar", "Pixie", "Psylocke", "Dr. Cecilia Reyes", "Rogue", "Shadowcat", "Storm", "Warbird", "Warpath", "Wolverine", "X-Man"].each do |name|
      Mutant.create :name => name
    end
  end

  desc "Run all init data tasks"
  task :all => [:mutants]
end
