class Task < ActiveRecord::Base
  has_many :team_tasks
  has_many :teams, :through => :team_tasks
  accepts_nested_attributes_for :team_tasks, :allow_destroy => true

  validates :name, :presence => true
  validates :name, :length => {:maximum => 255}
end
