class Team < ActiveRecord::Base
  has_many :team_tasks
  has_many :team_members
  has_many :mutants, :through => :team_members
  has_many :tasks, :through => :team_tasks
  accepts_nested_attributes_for :team_members, :allow_destroy => true

  validates :name, :presence => true
  validates :name, :length => {:maximum => 30}

  def self.search_for_task task, term, page
    exclude_ids = task ? task.team_ids : []
    if exclude_ids.count == 0
      teams = where("name like '%#{term}%'").paginate(:per_page => 10, :page => page)
    else
      teams = where("name like '%#{term}%' and id not in (?)", exclude_ids).paginate(:per_page => 10, :page => page)
    end
    teams.collect{|team| {:id => team.id, :name => team.name}}
  end
end
