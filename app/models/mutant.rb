class Mutant < ActiveRecord::Base
  has_many :team_members
  has_many :teams, :through => :team_members

  validates :name, :presence => true
  validates :name, :length => {:maximum => 30}
  validates :name, :format => {
    :with => /\A(\w|_)+\Z/,
    :message => "can only contain alphabets, numbers, and underscores(_)"
  }

  def self.search_for_team team, term, page
    exclude_ids = team ? team.mutant_ids : []
    if exclude_ids.count == 0
      mutants = where("name like '%#{term}%'").paginate(:per_page => 10, :page => page)
    else
      mutants = where("name like '%#{term}%' and id not in (?)", exclude_ids).paginate(:per_page => 10, :page => page)
    end
    mutants.collect{|mutant| {:id => mutant.id, :name => mutant.name}}
  end
end
