module ApplicationHelper
  def xmen_autocomplete_field name, placeholder, url, key
    text_field_tag name, nil, :class => 'xmen-fetch', :data => {:placeholder => placeholder, :url => url, :key => key }
  end
end
