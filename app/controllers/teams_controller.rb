class TeamsController < ApplicationController
  def index
    @teams = Team.all
  end

  def show
    @team = Team.find params[:id]
  end

  def new
    @team = Team.new
  end

  def create
    set_team_member_params
    @team = Team.new team_params
    if @team.valid?
      @team.save
      return redirect_to(teams_path, :notice => 'Team created successfully')
    else
      render :new
    end
  end

  def edit
    @team = Team.find params[:id]
  end

  def update
    @team = Team.find params[:id]
    set_team_member_params

    if @team.update_attributes team_params
      return redirect_to(teams_path, :notice => 'Team updated successfully')
    else
      render :edit
    end
  end

  def destroy
    @team = Team.find params[:id]
    @team.destroy
    return redirect_to(teams_path, :notice => 'Team deleted successfully')
  end

  def mutant_search
    team = params[:team_id] ? Team.find(params[:team_id]) : nil
    mutants = Mutant.search_for_team(team, params[:q], params[:page])
    respond_to do |format|
      format.json{render :json => mutants.to_json}
    end
  end

  private

  def team_params
    params.require(:team).permit(:name, :team_members_attributes => [:id, :mutant_id, :_destroy])
  end

  def set_team_member_params
    delete_mutants = params.delete :delete_mutants
    new_mutants    = params.delete :new_mutants

    params[:team][:team_members_attributes] = []

    if delete_mutants
      delete_mutants.each{|m| params[:team][:team_members_attributes] << {:id => m, :_destroy => true}}
    end
    if new_mutants.length > 0
      new_mutants.split(",").each{|m| params[:team][:team_members_attributes] << {:mutant_id => m}}
    end
  end
end
