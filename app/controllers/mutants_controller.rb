class MutantsController < ApplicationController
  def index
    @mutants = Mutant.all
  end

  def show
    @mutant = Mutant.find params[:id]
  end

  def new
    @mutant = Mutant.new
  end

  def create
    @mutant = Mutant.new mutant_params
    if @mutant.valid?
      @mutant.save
      return redirect_to(mutants_path, :notice => 'Mutant created successfully')
    else
      render :new
    end
  end

  def edit
    @mutant = Mutant.find params[:id]
  end

  def update
    @mutant = Mutant.find params[:id]
    if @mutant.update_attributes mutant_params
      return redirect_to(mutants_path, :notice => 'Mutant updated successfully')
    else
      render :edit
    end
  end

  def destroy
    @mutant = Mutant.find params[:id]
    @mutant.destroy
    return redirect_to(mutants_path, :notice => 'Mutant deleted successfully')
  end

  private

  def mutant_params
    params.require(:mutant).permit(:name)
  end
end
