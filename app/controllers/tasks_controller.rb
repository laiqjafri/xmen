class TasksController < ApplicationController
  def index
    @tasks = Task.all
  end

  def show
    @task = Task.find params[:id]
  end

  def new
    @task = Task.new
  end

  def create
    set_team_params
    @task = Task.new task_params
    if @task.valid?
      @task.save
      return redirect_to(tasks_path, :notice => 'Task created successfully')
    else
      render :new
    end
  end

  def edit
    @task = Task.find params[:id]
  end

  def update
    @task = Task.find params[:id]
    set_team_params
    if @task.update_attributes task_params
      return redirect_to(tasks_path, :notice => 'Task updated successfully')
    else
      render :edit
    end
  end

  def destroy
    @task = Task.find params[:id]
    @task.destroy
    return redirect_to(tasks_path, :notice => 'Task deleted successfully')
  end

  def team_search
    task = params[:task_id] ? Task.find(params[:task_id]) : nil
    teams = Team.search_for_task(task, params[:q], params[:page])
    respond_to do |format|
      format.json{render :json => teams.to_json}
    end
  end

  private

  def task_params
    params.require(:task).permit(:name, :team_tasks_attributes => [:id, :team_id, :_destroy])
  end

  def set_team_params
    delete_teams = params.delete :delete_teams
    new_teams    = params.delete :new_teams

    params[:task][:team_tasks_attributes] = []

    if delete_teams
      delete_teams.each{|t| params[:task][:team_tasks_attributes] << {:id => t, :_destroy => true}}
    end
    if new_teams.length > 0
      new_teams.split(",").each{|t| params[:task][:team_tasks_attributes] << {:team_id => t}}
    end
  end
end
