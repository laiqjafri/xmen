$(document).ready(function() {
  $(".xmen-fetch").each(function(i, item) {
    var display_key = $(item).data('key');
    var minimum_input = 1;
    $(item).select2({
        placeholder: $(item).data('placeholder'),
        minimumInputLength: minimum_input,
        width: 'element',
        allowClear: true,

        ajax: {
            url: $(item).data('url'),
            dataType: 'json',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, //search term
                    f: display_key,
                    page: page, // page number
                    format: 'json',
                };
            },
            results: function (data, page) {
                var more = data.length >= 10; // whether or not there are more results available
                return {results: data, more: more};
            },
        },

        formatResult: function(res){return res[display_key]}, // omitted for brevity, see the source of this page
        formatSelection: function(res){return res[display_key]}, // omitted for brevity, see the source of this page
        multiple: true,
        dropdownCssClass: "bigdrop",

        initSelection: function(elem, callback) {
          var data = [];
          var initial_data = eval('()');
          $.each(initial_data, function(key, value) {
            var obj = {};
            obj[display_key] = value;
            obj["id"] = key;
            data.push(obj);
          });
          if(!multi_select && data.length > 0) { data = data[0]; } //return single object if not multi select
          callback(data);
        },
    });
  });
});

