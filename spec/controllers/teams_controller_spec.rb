require 'spec_helper'

describe TeamsController, :type => :controller do
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "loads all of the teams into @posts" do
      Team.destroy_all
      team1, team2 = Team.create(:name => "TeamX"), Team.create(:name => "TeamY")
      get :index

      expect(assigns(:teams)).to match_array([team1, team2])
    end
  end

  describe "GET #mutant_search" do
    it "responsds to mutant_search call" do
      expect(Mutant).to receive(:search_for_team).once
      get :mutant_search, :q => 'Cyc', :format => 'json'
    end
  end
end
