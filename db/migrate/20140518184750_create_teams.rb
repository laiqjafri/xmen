class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name, limit: 30

      t.timestamps
    end
    add_index :teams, :name
  end
end
