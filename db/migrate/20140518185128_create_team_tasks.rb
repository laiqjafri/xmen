class CreateTeamTasks < ActiveRecord::Migration
  def change
    create_table :team_tasks do |t|
      t.integer :team_id
      t.integer :task_id

      t.timestamps
    end
    add_index :team_tasks, :team_id
    add_index :team_tasks, :task_id
  end
end
