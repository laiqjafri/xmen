class AddUniqueIndexesToXrefs < ActiveRecord::Migration
  def change
    add_index :team_members, [:team_id, :mutant_id], :unique => true
    add_index :team_tasks, [:team_id, :task_id], :unique => true
  end
end
