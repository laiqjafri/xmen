class CreateMutants < ActiveRecord::Migration
  def change
    create_table :mutants do |t|
      t.string :name, limit: 30

      t.timestamps
    end
    add_index :mutants, :name
  end
end
