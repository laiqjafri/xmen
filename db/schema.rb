# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140519092542) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "mutants", force: true do |t|
    t.string   "name",       limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mutants", ["name"], name: "index_mutants_on_name", using: :btree

  create_table "tasks", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tasks", ["name"], name: "index_tasks_on_name", using: :btree

  create_table "team_members", force: true do |t|
    t.integer  "team_id"
    t.integer  "mutant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "team_members", ["mutant_id"], name: "index_team_members_on_mutant_id", using: :btree
  add_index "team_members", ["team_id", "mutant_id"], name: "index_team_members_on_team_id_and_mutant_id", unique: true, using: :btree
  add_index "team_members", ["team_id"], name: "index_team_members_on_team_id", using: :btree

  create_table "team_tasks", force: true do |t|
    t.integer  "team_id"
    t.integer  "task_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "team_tasks", ["task_id"], name: "index_team_tasks_on_task_id", using: :btree
  add_index "team_tasks", ["team_id", "task_id"], name: "index_team_tasks_on_team_id_and_task_id", unique: true, using: :btree
  add_index "team_tasks", ["team_id"], name: "index_team_tasks_on_team_id", using: :btree

  create_table "teams", force: true do |t|
    t.string   "name",       limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "teams", ["name"], name: "index_teams_on_name", using: :btree

end
